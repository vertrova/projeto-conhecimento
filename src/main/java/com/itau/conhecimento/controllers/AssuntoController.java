package com.itau.conhecimento.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.conhecimento.models.Assunto;
import com.itau.conhecimento.repositories.AssuntoRepository;

@RestController
public class AssuntoController {
	
	@Autowired
	AssuntoRepository assuntoRepository;
	
	@RequestMapping(method = RequestMethod.GET, path="/api/v1/assuntos")
	public Iterable<Assunto> consultarAssunto() {		
//oiee
		return assuntoRepository.findAll();

	}
	

}
