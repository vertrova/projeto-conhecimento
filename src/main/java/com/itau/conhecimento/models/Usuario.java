package com.itau.conhecimento.models;

import org.hibernate.search.annotations.Field;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Usuario {

	@Id
	@GeneratedValue
	private long id;
	@NotNull
	@Field
	private String nome;
	@NotNull
	private String email;
	private String telefone;
	private String senha;

//	@OneToMany(mappedBy="usuario")
//	private List<Conhecimento> conhecimentos;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

//	public List<Conhecimento> getConhecimentos() {
//		return conhecimentos;
//	}
//
//	public void setConhecimentos(List<Conhecimento> conhecimentos) {
//		this.conhecimentos = conhecimentos;
//	}

}
