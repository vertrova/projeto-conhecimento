package com.itau.conhecimento;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan("com.itau.conhecimento.*")
public class App {
    public static void main(String[] args) {
	//Ola
        SpringApplication.run(App.class, args);

    }
}
