package com.itau.conhecimento.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.itau.conhecimento.models.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Long> {

	public Optional<Usuario> findByEmail(String email);
	
}


