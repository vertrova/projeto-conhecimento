package com.itau.conhecimento.controllers;

//import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.conhecimento.models.Conhecimento;
import com.itau.conhecimento.repositories.ConhecimentoRepository;

@RestController
public class ConhecimentoController {

	@Autowired
	private ConhecimentoRepository conhecimentoRepository;
	
	

	@RequestMapping(method = RequestMethod.POST, path = "/api/v1/incluirConhecimento")
	public Conhecimento criarConhecimento(@RequestBody Conhecimento conhecimento) {

//		LocalDate dataAtual = LocalDate.now();
//		conhecimento.setData(dataAtual);

		return conhecimentoRepository.save(conhecimento);

	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/api/v1/alterarConhecimento")
	public Conhecimento alterarConhecimento(@RequestBody Conhecimento conhecimento) {

		return conhecimentoRepository.save(conhecimento);

	}

	@RequestMapping(method = RequestMethod.POST, path = "/api/v1/excluirConhecimento")
	public ResponseEntity<?> excluirConhecimento(@RequestBody Conhecimento conhecimento) {

		conhecimentoRepository.delete(conhecimento.getId());
		return ResponseEntity.ok().build();

	}

}
