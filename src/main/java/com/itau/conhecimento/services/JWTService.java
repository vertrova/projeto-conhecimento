package com.itau.conhecimento.services;

import java.lang.reflect.Type;

import org.springframework.stereotype.Service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.google.gson.Gson;

@Service
public class JWTService<T> {

	String key = "compartilhandoconhecimento";
	Algorithm algorithm = Algorithm.HMAC256(key);
	JWTVerifier verifier = JWT.require(algorithm).build();
	
	public String gerarToken(T t) {

		Gson gson = new Gson();
		String s = gson.toJson(t);

		return JWT.create().withClaim("objeto", s).sign(algorithm);
	}

	public T verificarToken(String token, T t) {

		Gson gson = new Gson();

		T obj;
		obj = (T) gson.fromJson(verifier.verify(token).getClaim("objeto").asString(), t.getClass());

		return obj;
	}
}
