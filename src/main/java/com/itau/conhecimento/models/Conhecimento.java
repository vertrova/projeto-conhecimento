package com.itau.conhecimento.models;

import org.hibernate.search.annotations.DocumentId;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Indexed
public class Conhecimento {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @DocumentId
    private long Id;

    @NotNull
    @ManyToOne
    @IndexedEmbedded
    private Usuario usuario;

    @NotNull
    @ManyToOne
    private Assunto assunto;

    @Field
    private String titulo;

    @Field
    private String descricao;
    @Temporal(TemporalType.DATE)
    private java.util.Date data;

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    @NotNull
    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(@NotNull Usuario usuario) {
        this.usuario = usuario;
    }

    @NotNull
    public Assunto getAssunto() {
        return assunto;
    }

    public void setAssunto(@NotNull Assunto assunto) {
        this.assunto = assunto;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }
}
