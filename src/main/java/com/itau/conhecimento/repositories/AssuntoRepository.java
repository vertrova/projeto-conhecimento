package com.itau.conhecimento.repositories;

import org.springframework.data.repository.CrudRepository;

import com.itau.conhecimento.models.Assunto;

public interface AssuntoRepository extends CrudRepository<Assunto, Long> {

}
