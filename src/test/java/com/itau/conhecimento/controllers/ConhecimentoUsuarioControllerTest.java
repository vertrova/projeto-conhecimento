package com.itau.conhecimento.controllers;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import java.util.ArrayList;
import java.util.List;



import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.internal.matchers.InstanceOf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.itau.conhecimento.configuration.HibernateSearchConfiguration;
import com.itau.conhecimento.models.Conhecimento;
import com.itau.conhecimento.models.Usuario;
import com.itau.conhecimento.repositories.AssuntoRepository;
import com.itau.conhecimento.repositories.ConhecimentoRepository;
import com.itau.conhecimento.repositories.UsuarioRepository;
import com.itau.conhecimento.services.HibernateSearchService;

@RunWith(SpringRunner.class)
@WebMvcTest(ConhecimentoUsuarioController.class)
public class ConhecimentoUsuarioControllerTest {

	@MockBean
	private HibernateSearchService searchservice;
	
	@MockBean
	private UsuarioRepository usuarioRepository;
	
	@MockBean
	private ConhecimentoRepository conhecimentoRepository;
	
	@MockBean
	private AssuntoRepository assuntoRepository;
	
	@MockBean
	HibernateSearchConfiguration hibernateSearchConfiguration;
	
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void buscarConhecimento() throws Exception {
		List<Conhecimento> lista = new ArrayList<>();
		Conhecimento conhecimento = new Conhecimento();
		conhecimento.setTitulo("Java");
		lista.add(conhecimento);
		
		when(
				searchservice.fuzzySearch(
						(String) argThat(new InstanceOf(String.class))
				)
		).thenReturn(lista);

		mockMvc.perform(get("/api/v1/conhecimentos?search=teste")
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$[0].titulo", equalTo("Java")));
	}

}
