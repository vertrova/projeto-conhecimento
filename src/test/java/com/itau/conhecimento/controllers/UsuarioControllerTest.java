package com.itau.conhecimento.controllers;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.internal.matchers.InstanceOf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itau.conhecimento.configuration.HibernateSearchConfiguration;
import com.itau.conhecimento.models.Conhecimento;
import com.itau.conhecimento.models.Usuario;
import com.itau.conhecimento.repositories.AssuntoRepository;
import com.itau.conhecimento.repositories.ConhecimentoRepository;
import com.itau.conhecimento.repositories.UsuarioRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(UsuarioController.class)
public class UsuarioControllerTest {
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private UsuarioRepository usuarioRepository;
	
	@MockBean
	private ConhecimentoRepository conhecimentoRepository;
	
	@MockBean
	private AssuntoRepository assuntoRepository;

	@MockBean
	HibernateSearchConfiguration hibernateSearchConfiguration;
	
	ObjectMapper mapper = new ObjectMapper();
	
	@Test
	public void incluirUsuario() throws Exception {
		Usuario usuario = new Usuario();
		usuario.setNome("Carlos Mendes");
		usuario.setSenha("vaicurintia");
			
		
		String json = mapper.writeValueAsString(usuario);
		
		when(
				usuarioRepository.save(
						(Usuario) argThat(new InstanceOf(Usuario.class))
				)
		).thenReturn(usuario);

		mockMvc.perform(post("/api/v1/incluirUsuario")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.nome", equalTo("Carlos Mendes")));
	}
}
