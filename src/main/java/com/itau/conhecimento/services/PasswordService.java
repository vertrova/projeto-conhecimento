package com.itau.conhecimento.services;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class PasswordService {

	BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

	public String setHash(String password) {
		return encoder.encode(password);
	}
	
	public boolean verifyHash(String password, String hash) {
		return encoder.matches(password, hash);
	}
}