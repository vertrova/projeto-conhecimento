package com.itau.conhecimento.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class Assunto {

    @Id
    private long id;
    private String nome;

//    @OneToMany(mappedBy = "assunto")
//    private List<Conhecimento> conhecimentos;

    public long getId() {
        return id;
    }


    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

//    public List<Conhecimento> getConhecimentos() {
//        return conhecimentos;
//    }
//
//    public void setConhecimentos(List<Conhecimento> conhecimentos) {
//        this.conhecimentos = conhecimentos;
//    }

}
