package com.itau.conhecimento.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.itau.conhecimento.models.Conhecimento;
import com.itau.conhecimento.services.HibernateSearchService;

@RestController
public class ConhecimentoUsuarioController {

	@Autowired
	private HibernateSearchService searchservice;

	@RequestMapping(value = "/api/v1/conhecimentos", method = RequestMethod.GET)
	public ResponseEntity<?> search(@RequestParam("buscar-por") String q) throws Exception {
		List<Conhecimento> searchResults = null;
		try {
			searchResults = searchservice.fuzzySearch(q);

		} catch (Exception ex) {

			throw ex;
		}
		// model.addAttribute("search", searchResults);
		return ResponseEntity.ok().body(searchResults);
	}
}
