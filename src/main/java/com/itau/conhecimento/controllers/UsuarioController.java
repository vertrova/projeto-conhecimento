package com.itau.conhecimento.controllers;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.conhecimento.models.Usuario;
import com.itau.conhecimento.repositories.UsuarioRepository;
import com.itau.conhecimento.services.JWTService;
import com.itau.conhecimento.services.PasswordService;

@RestController
public class UsuarioController {

	@Autowired
	UsuarioRepository usuarioRepository;

	@Autowired
	JWTService<Usuario> jwtService;
	
	@Autowired
	PasswordService passwordService;

	@RequestMapping(method = RequestMethod.POST, path = "/api/v1/incluirUsuario")
	public Usuario criarUsuario(@RequestBody Usuario usuario) {

		usuario.setSenha(passwordService.setHash(usuario.getSenha()));

		return usuarioRepository.save(usuario);

	}

	@RequestMapping(method = RequestMethod.POST, path = "/api/v1/login")
	public ResponseEntity<?> buscarUsuario(@RequestBody Usuario usuario) {

		Optional<Usuario> optionalUsuario = usuarioRepository.findByEmail(usuario.getEmail());

		if (!optionalUsuario.isPresent()) {
			return ResponseEntity.notFound().build();
		}

		Usuario usuarioBanco = optionalUsuario.get();
//		usuarioBanco.setConhecimentos(null);

		if (passwordService.verifyHash(usuario.getSenha(), usuarioBanco.getSenha())) {
			String token = jwtService.gerarToken(usuarioBanco);

			HttpHeaders headers = new HttpHeaders();

			headers.add("Authorization", String.format("bearer %s", token));
			headers.add("Acess-Control-Expose-Headers", "Authorization");

			return new ResponseEntity<Usuario>(usuarioBanco, headers, HttpStatus.OK);

		}

		return ResponseEntity.notFound().build();

	}

	@RequestMapping(method = RequestMethod.GET, path = "/api/v1/buscarUsuarioConhecimentos")
	public ResponseEntity<Usuario> buscarUsuario(HttpServletRequest request) {

		String token = request.getHeader("Authorization");
		token = token.replace("Bearer ", "");

		Usuario usuarioGenerico = new Usuario();

		Usuario usuario = (Usuario) jwtService.verificarToken(token, usuarioGenerico);

		if (usuario == null) {
			ResponseEntity.notFound().build();
		}

		Optional<Usuario> optionalUsuario = usuarioRepository.findByEmail(usuario.getEmail());

		if (!optionalUsuario.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		usuario = optionalUsuario.get();
		
		return ResponseEntity.ok().body(usuario);
	}

}
