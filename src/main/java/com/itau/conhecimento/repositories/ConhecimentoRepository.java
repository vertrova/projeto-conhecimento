package com.itau.conhecimento.repositories;

import org.springframework.data.repository.CrudRepository;

import com.itau.conhecimento.models.Conhecimento;

public interface ConhecimentoRepository extends CrudRepository<Conhecimento, Long> {

}
